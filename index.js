const { topPosts, otherPosts, dailyTopPosts } = require('./lib/queries');
const App = require('./lib/app');
const [,, inputFile, opts] = process.argv;
const app = new App({
    inputFile,
    opts,
    jobs: [
        {
            query: topPosts,
            fileName: 'top_posts'
        },
        {
            query: otherPosts,
            fileName: 'other_posts'
        },
        {
            query: dailyTopPosts,
            fileName: 'daily_top_posts'
        }
    ]
})

app.start()