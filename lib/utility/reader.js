const fs = require('fs');
const { parseCSV } = require('./csv')

async function readFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, {encoding: 'utf8'}, (err, data) => {
            if (err) reject(err)
            resolve(data)
        })
    })
}

async function readCSV(filePath) {
    return readFile(filePath).then(parseCSV)
}

async function readJSON(filePath) {
    return readFile(filePath).then(JSON.parse)
}

async function read(filePath) {
    const path = require('path')
    const ext = path.extname(filePath)
    if (ext === '.csv') {
        console.info('reading CSV')
        return readCSV(filePath).then((data) => {
            console.info(data.info);
            return data.records;
        });
    }
    console.info('reading JSON')
    return readJSON(filePath)
}

module.exports = { read, readFile, readCSV, readJSON }