const fs = require('fs');
const { toCSV } = require('./csv')
function writeFile(filePath, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, data, {encoding: 'utf8'}, (err) => {
            if (err) reject(err)
            resolve(filePath)
        })
    })
}

function writeJSON(filePath, data) {
    const input = JSON.stringify(data, null, ' ')
    return writeFile(filePath, input)
}

function writeCSV(filePath, data) {
    const csv = toCSV(data)
    return writeFile(filePath, csv)
}

function write(filePath, data, inJSON) {
    if (inJSON) {
        return writeJSON(filePath + '.json', data)
    }
    return writeCSV(filePath + '.csv', data)
}

module.exports = { write }