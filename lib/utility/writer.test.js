jest.mock('fs')
const fs = require('fs');
const writer = require('./writer')

test.only('write CSV', () => {
    const testData = [{
        column: 'row'
    }]
    fs.writeFile.mockImplementationOnce((f, d, o, cb) => {
        cb(null)
    });
    const result = writer.write('test_name', testData, null)
    expect(result).resolves.toEqual('test_name.csv')
})

test.only('write JSON', () => {
    const testData = [{
        column: 'row'
    }]
    fs.writeFile.mockImplementationOnce((f, d, o, cb) => {
        cb(null)
    });
    const result = writer.write('test_name', testData, true)
    expect(result).resolves.toEqual('test_name.json')
})