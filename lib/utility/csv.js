
async function parseCSV(data) {
    const parse = require('csv-parse')
    return new Promise((resolve, reject) => {
        parse(data, {
            delimiter: ',',
            cast: true,
            relax: true,
            columns: true,
            cast_date: false,
          }, (err, records, info) => {
            if (err) reject(err)
            resolve({ records, info })
          })
    })
}

function toCSV(data) {
    if (data.length === 0) {
        return [];
    }
    const headers = Object.keys(data[0]).join(',');
    const rest = data.map(convert);
    return [
        headers,
        ...rest,
    ].join('\n')
}

function convert(obj) {
    return Object.values(obj).map(item => {
        if (typeof item === 'string' && item.indexOf(',') !== -1) {
            return `"${item}"`
        }
        return item
    }).join(',')
}

module.exports = { parseCSV, toCSV }
