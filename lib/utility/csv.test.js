const { parseCSV, toCSV } = require('./csv')

test('toCSV', () => {
    expect(toCSV([{
        column1: 0,
        column2: "a string"
    }])).toEqual("column1,column2\n0,a string");

    expect(toCSV([{
        header: true,
        another: "a comma, in a string"
    }])).toStrictEqual(`header,another\ntrue,"a comma, in a string"`);
})

test('parseCSV', () => {
    const testData = `boolean,string,number,stringWithComma\ntrue,hello world,43,"hello, world"`
    const results = parseCSV(testData)
    expect(results).resolves.toStrictEqual({
        records: [
            {
                boolean: "true",
                string: 'hello world',
                number: 43,
                stringWithComma: 'hello, world'
            }
        ],
        info: {
            comment_lines: 0,
            empty_lines: 0,
            invalid_field_length: 0,
            lines: 2,
            records: 1
        }
    })
})