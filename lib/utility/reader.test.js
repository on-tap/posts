jest.mock('fs')
jest.mock('csv-parse')
const parse = require('csv-parse')
const fs = require('fs')

const reader = require('./reader');

test('read CSV', () => {
    fs.readFile.mockImplementationOnce((f, o, cb) => {
        cb(null, 'column\nrow')
    });
    parse.mockImplementationOnce((d, o, cb) => {
        cb(null, [ { column: 'row' }], { lines: 2, records: 1})
    })
    const output = reader.read('test.csv')

    expect(output).resolves.toEqual([
        {
            column: 'row'
        }
    ])
})
test('read JSON', () => {
    fs.readFile.mockImplementationOnce((f, o, cb) => {
        cb(null, `[{"column": "row"}]`)
    });
    const output = reader.read('test.json')
    expect(output).resolves.toEqual([
        {
            column: 'row'
        }
    ])
})