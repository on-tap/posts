const queries = require('./index')

test('topPosts and otherPosts', () => {
    const testData = [
        {
            privacy: 'public',
            comments: 11,
            views: 9001,
            title: "What We Know About The App That Delayed"
        },
        {
            privacy: 'public',
            comments: 110,
            views: 10203,
            title: "How Google Earth Really works"
        },
        {
            privacy: 'private',
            comments: 2,
            views: 3,
            title: "My First Post"
        },
    ];
    const topPosts = queries.topPosts(testData)
    expect(topPosts).toEqual([
        {
            privacy: 'public',
            comments: 11,
            views: 9001,
            title: "What We Know About The App That Delayed"
        },
        {
            privacy: 'public',
            comments: 110,
            views: 10203,
            title: "How Google Earth Really works"
        },
    ])
    const otherPosts = queries.otherPosts(testData)
    expect(otherPosts).toEqual([{
        privacy: 'private',
        comments: 2,
        views: 3,
        title: "My First Post"
    }])
})

test('dailyTopPosts', () => {
    const testData = [
        {
            id: 3,
            likes: 34,
            timestamp: new Date('07/08/63')
        },
        {
            id: 2,
            likes: 100,
            timestamp: new Date('07/08/63')
        },
        {
            id: 7,
            likes: 78,
            timestamp: new Date('03/20/09')
        }
    ]
    const dailyTopPosts = queries.dailyTopPosts(testData)
    expect(dailyTopPosts).toEqual([
        {
            id: 7,
            likes: 78,
            timestamp: new Date('03/20/09')
        },
        {
            id: 2,
            likes: 100,
            timestamp: new Date('07/08/63')
        }
    ])
})

test('mostLikes', () => {
    const testData = [
        {
            id: 4,
            likes: 232
        },
        {
            id: 9,
            likes: 10000
        },
        {
            id: 18,
            likes: 381
        },
    ]
    const mostLikes = queries.mostLikes(testData)
    expect(mostLikes).toEqual({
        id: 9,
        likes: 10000
    })
})