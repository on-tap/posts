
function byDate(a, b) {
    return b.timestamp - a.timestamp;
}

function byLikes(a, b) {
    return b.likes > a.likes;
}

module.exports = { byDate, byLikes }