const { byDate, byLikes } = require('./sorters')
const { mapByDate } = require('./transforms')
const filters = require('./filters')

function topPosts(data) {
    return data.filter(filters.topPosts);
}

function otherPosts(data) {
    return data.filter((value) => {
        return !filters.topPosts(value)
    });
}

function dailyTopPosts(data) {
    let datesMap = mapByDate(data);
    let dailyTopPosts = [];
    for (let date of Object.keys(datesMap)) {
        let posts = datesMap[date]
        dailyTopPosts.push(mostLikes(posts))
    }
    return dailyTopPosts.sort(byDate);
}

function mostLikes(data) {
    return data.sort(byLikes)[0]
}
 
module.exports = { topPosts, otherPosts, dailyTopPosts, mostLikes };