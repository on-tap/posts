const conditions = require('./conditions');

test('isPublic', () => {
    expect(conditions.isPublic({ 
        privacy: 'public' 
    })).toBeTruthy()
    expect(conditions.isPublic({ 
        privacy: 'private' 
    })).toBeFalsy()
})

test('commentGreaterThan', () => {
    const isCommentGreaterThanLimit = conditions.commentGreaterThan(3)
    expect(isCommentGreaterThanLimit({
        comments: 4
    })).toBeTruthy()
    expect(isCommentGreaterThanLimit({
        comments: 3
    })).toBeFalsy()
    expect(isCommentGreaterThanLimit({
        comments: 2
    })).toBeFalsy()
})

test('viewsGreaterThan', () => {
    const isViewsGreaterThanLimit = conditions.viewsGreaterThan(5)
    expect(isViewsGreaterThanLimit({
        views: 6
    })).toBeTruthy()
    expect(isViewsGreaterThanLimit({
        views: 5
    })).toBeFalsy()
    expect(isViewsGreaterThanLimit({ 
        views: 4
    })).toBeFalsy()
})

test('titleLengthLessThan', () => {
    const isTitleLessThanLimit = conditions.titleLessThan(3)
    expect(isTitleLessThanLimit({
        title: 'ab'
    })).toBeTruthy()
    expect(isTitleLessThanLimit({
        title: 'abc'
    })).toBeFalsy()
    expect(isTitleLessThanLimit({
        title: 'abcd'
    })).toBeFalsy()
})