const { commentGreaterThan, viewsGreaterThan, titleLessThan, isPublic} = require('./conditions');

const hasMoreThan10Comments = commentGreaterThan(10)
const hasMoreThan9000Views = viewsGreaterThan(9000);
const isTitleLessThan40Chars = titleLessThan(40);

function topPosts(post) {
    return isPublic(post) && 
        hasMoreThan10Comments(post) && 
        hasMoreThan9000Views(post) && 
        isTitleLessThan40Chars(post);
}

module.exports = { topPosts }
