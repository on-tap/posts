const transforms = require('./transforms');

test('mapByDate', () => {
    const data = [
        {
            id: 'bob',
            timestamp: new Date('04/04/04')
        },
        {
            id: 'tom',
            timestamp: new Date('07/07/07')
        },
        {
            id: 'nancy',
            timestamp: '2004-04-04T13:44:02.000Z'
        }
    ]
    const map = transforms.mapByDate(data)
    expect(map).toStrictEqual({
        '2004-4-4': [
            {
                id: 'bob',
                timestamp: new Date('04/04/04')
            },
            {
                id: 'nancy',
                timestamp: '2004-04-04T13:44:02.000Z'
            },
        ],
        '2007-7-7': [
            {
                id: 'tom',
                timestamp: new Date('07/07/07')
            }
        ]
    })
})
