function mapByDate(data) {
    const days = {};
    let date;
    data.forEach((value) => {
        if (value.timestamp.constructor !== Date) {
            date = (new Date(value.timestamp)).toLocaleDateString()
        } else {
            date = value.timestamp.toLocaleDateString();
        }
        days[date] = days[date] || [];
        days[date].push(value);
    })
    return days;
}

module.exports = { mapByDate }