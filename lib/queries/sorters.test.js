const sorters = require('./sorters');

test('byDate', () => {
    const unsorted = [
        {
            timestamp: new Date('01/02/2020')
        },
        {
            timestamp: new Date('01/03/2020')
        },
        {
            timestamp: new Date('01/01/2020')
        },
    ];
    const sorted = unsorted.sort(sorters.byDate)
    expect(sorted).toEqual([
        {
            timestamp: new Date('01/03/2020')
        },
        {
            timestamp: new Date('01/02/2020')
        },
        {
            timestamp: new Date('01/01/2020')
        },
    ])
})

test('byLikes', () => {
    const unsorted = [
        {
            likes: 0
        },
        {
            likes: 4
        }, 
        {
            likes: 2
        }
    ];
    const sorted = unsorted.sort(sorters.byLikes)
    expect(sorted).toEqual([
        {
            likes: 4
        },
        {
            likes: 2
        }, 
        {
            likes: 0
        }
    ])
})