function isPublic(record) {
    return record.privacy === 'public';
}

function commentGreaterThan(comments) {
    return record => record.comments > comments
}

function viewsGreaterThan(views) {
    return record => record.views > views
}

function titleLessThan(length) {
    return record => record.title.length < length
}

module.exports = { isPublic, commentGreaterThan, viewsGreaterThan, titleLessThan }