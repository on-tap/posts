const filters = require('./filters')

test('topPosts', () => {
    expect(filters.topPosts({
        privacy: 'public',
        comments: 11,
        views: 9001,
        title: "What We Know About The App That Delayed"
    })).toBeTruthy()

    expect(filters.topPosts({
        privacy: 'private',
        comments: 11,
        views: 9001,
        title: "What We Know About The App That Delayed"
    })).toBeFalsy()

    expect(filters.topPosts({
        privacy: 'public',
        comments: 10,
        views: 9001,
        title: "What We Know About The App That Delayed"
    })).toBeFalsy()

    expect(filters.topPosts({
        privacy: 'public',
        comments: 11,
        views: 9000,
        title: "What We Know About The App That Delayed"
    })).toBeFalsy()

    expect(filters.topPosts({
        privacy: 'public',
        comments: 11,
        views: 9001,
        title: "What We Know About The App That Delayed Iowa's Caucus Results"
    })).toBeFalsy()
})