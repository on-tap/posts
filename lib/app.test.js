const App = require('./app')
const { topPosts, otherPosts, dailyTopPosts } = require('./queries');

const fs = require('fs')
const rimraf = require('rimraf')

describe('app', () => {
    beforeEach((done) => {
        rimraf('./lib/_test', {}, (err) => {
            fs.mkdirSync('./lib/_test')
            done()
        })
    })

    test('csv to json', async () => {
        const app = new App({
            inputFile: './test/posts.csv',
            opts: '--json',
            jobs: [
                {
                    query: topPosts,
                    fileName: './lib/_test/top_posts'
                },
                {
                    query: otherPosts,
                    fileName: './lib/_test/other_posts'
                },
                {
                    query: dailyTopPosts,
                    fileName: './lib/_test/daily_top_posts'
                }
            ]
        })
        await app.start()
        
        let contents = fs.readFileSync('./lib/_test/top_posts.json', { encoding: 'utf8'})
        let data = JSON.parse(contents)
        expect(data.length).toBe(346)

        contents = fs.readFileSync('./lib/_test/other_posts.json', { encoding: 'utf8'})
        data = JSON.parse(contents)
        expect(data.length).toBe(1639)

        contents = fs.readFileSync('./lib/_test/daily_top_posts.json', { encoding: 'utf8'})
        data = JSON.parse(contents)
        expect(data.length).toBe(19)
    })

    test('csv to csv', async () => {
        const app = new App({
            inputFile: './test/posts.csv',
            opts: '',
            jobs: [
                {
                    query: topPosts,
                    fileName: './lib/_test/top_posts'
                },
                {
                    query: otherPosts,
                    fileName: './lib/_test/other_posts'
                },
                {
                    query: dailyTopPosts,
                    fileName: './lib/_test/daily_top_posts'
                }
            ]
        })
        await app.start()
        
        let contents = fs.readFileSync('./lib/_test/top_posts.csv', { encoding: 'utf8'})
        let rowCount = contents.split('\n').length
        expect(rowCount).toBe(347)

        contents = fs.readFileSync('./lib/_test/other_posts.csv', { encoding: 'utf8'})
        rowCount = contents.split('\n').length
        expect(rowCount).toBe(1640)

        contents = fs.readFileSync('./lib/_test/daily_top_posts.csv', { encoding: 'utf8'})
        rowCount = contents.split('\n').length
        expect(rowCount).toBe(20)
    })

    test('json to csv', async () => {
        debugger;
        const app = new App({
            inputFile: './test/posts.json',
            opts: '',
            jobs: [
                {
                    query: topPosts,
                    fileName: './lib/_test/top_posts'
                },
                {
                    query: otherPosts,
                    fileName: './lib/_test/other_posts'
                },
                {
                    query: dailyTopPosts,
                    fileName: './lib/_test/daily_top_posts'
                }
            ]
        })
        await app.start()
        
        let contents = fs.readFileSync('./lib/_test/top_posts.csv', { encoding: 'utf8'})
        let rowCount = contents.split('\n').length
        expect(rowCount).toBe(347)

        contents = fs.readFileSync('./lib/_test/other_posts.csv', { encoding: 'utf8'})
        rowCount = contents.split('\n').length
        expect(rowCount).toBe(1640)

        contents = fs.readFileSync('./lib/_test/daily_top_posts.csv', { encoding: 'utf8'})
        rowCount = contents.split('\n').length
        expect(rowCount).toBe(20)
    })

    test('json to json', async () => {
        const app = new App({
            inputFile: './test/posts.json',
            opts: '--json',
            jobs: [
                {
                    query: topPosts,
                    fileName: './lib/_test/top_posts'
                },
                {
                    query: otherPosts,
                    fileName: './lib/_test/other_posts'
                },
                {
                    query: dailyTopPosts,
                    fileName: './lib/_test/daily_top_posts'
                }
            ]
        })
        await app.start()
        
        let contents = fs.readFileSync('./lib/_test/top_posts.json', { encoding: 'utf8'})
        let data = JSON.parse(contents)
        expect(data.length).toBe(346)

        contents = fs.readFileSync('./lib/_test/other_posts.json', { encoding: 'utf8'})
        data = JSON.parse(contents)
        expect(data.length).toBe(1639)

        contents = fs.readFileSync('./lib/_test/daily_top_posts.json', { encoding: 'utf8'})
        data = JSON.parse(contents)
        expect(data.length).toBe(19)
    })
})
