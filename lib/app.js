
const { read } = require('./utility/reader')
const { write } = require('./utility/writer');

class App {
    constructor(config) {
        this.inputFile = config.inputFile
        this.jobs = config.jobs
        this.fileOutputConfig = {
            isJSON: config.opts === '--json'
        }
    }

    async start() {
        console.info('starting app')
        let data;
        try {
            data = await read(this.inputFile)
        } catch (err) {
            console.error('failed to read input file', this.inputFile, err)
            return;
        }   
        await this.processQueries(data)
    }

    async processQueries(data) {
        return Promise.all(this.jobs.map(async job => {
            try {
                console.info(`executing query ${job.query.name}`);
                const results = job.query(data)
                const file = await write(job.fileName, results, this.fileOutputConfig.isJSON)
                console.info('finished writing', file)
            } catch (err) {
                console.error(`failed to execute query ${job.query.name} for file ${job.fileName}`, err)
            }
        }))
    }
}

module.exports = App