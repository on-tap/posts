# Posts

## Install


```
    npm install -P
```

## Run

```
    npm start -- [filePath] [--json]

Options:
    --json  Output results in JSON (defaults to CSV)
```
##### Supports JSON or CSV files

## Test
```
    npm test
```
##### Uses Jest testing framework

## Examples

```
    npm start -- test/posts.csv
```
```
    npm start -- test/posts.json
```

## App Configuration

```typescript
interface AppConfig {
     // file path to read from (.csv, .json)
    inputFile: string;
    // right now only supporting --json flag
    opts: string;
    // list of job configs
    jobs: []JobConfig
}

interface JobConfig {
        // query functions defined in lib/queries e.g. topPosts
        query: QueryFunction;
        // name of file to write results
        fileName: string;
}

type QueryFunction = (data: []any) => []any;
```

